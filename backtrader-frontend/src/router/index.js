import { createRouter, createWebHistory } from "vue-router";

const routes = [
  {
    path: "/signin",
    name: "SignIn",
    component: () => import("../components/Authentication/SignIn.vue"),
  },
  {
    path: "/signup",
    name: "SignUp",
    component: () => import("../components/Authentication/SignUp.vue"),
  },
  {
    path: "/verification",
    name: "Verification",
    component: () => import("../components/Authentication/Verification.vue"),
  },
  {
    path: "/dashboardinfo",
    name: "DashboardInfo",
    component: () => import("../components/Dashboard/DashboardInfo.vue"),
  },
  {
    path: "/watchlist",
    name: "Watchlist",
    component: () => import("../components/Dashboard/Watchlist.vue"),
  },
  {
    path: "/buy-sell",
    name: "BuySell",
    component: () => import("../components/Dashboard/BuySell.vue"),
  },
  {
    path: "/orders",
    name: "Orders",
    component: () => import("../components/Orders/Orders.vue"),
  },
  {
    path: "/profile",
    name: "Profile",
    component: () => import("../components/Profile/Profile.vue"),
  },
  {
    path: "/payments",
    name: "Payments",
    component: () => import("../components/Payments/Payments.vue"),
  },
  {
    path: "/signout",
    name: "SignOut",
    component: () => import("../components/Authentication/SignOut.vue"),
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
