# Backtrader Application

This is a full-stack application built with Backtrader, MongoDB, Vue.js, and Flask, docker for deployment. It allows users to perform trading simulations using historical market data.

## Features

- User registration and authentication
- Watchlist management for tracking stocks
- Buying and selling stocks in the simulation
- Dashboard to view account balance, portfolio, and performance metrics

## Prerequisites

- Python 3.9 or later
- MongoDB database
- Node.js and npm (for Vue.js frontend)

## Installation

### Backend

1. Clone the repository:
   git clone {repourl}

2. Navigate to the backend directory:
   cd backtrader-backend

3. Create and activate a virtual environment:
   py3 -m venv venv

4. Install the Python dependencies:
   pip install -r requirements.txt

5. Set up the MongoDB connection:

- Create a `mongo.py` file in the `backend/db` directory (refer to the previous instructions for creating the `mongo.py` file).
- Adjust the MongoDB connection parameters in `mongo.py` based on your MongoDB setup.

6. Start the Flask server:
   flask run

### Frontend

1. Navigate to the frontend directory:
   cd backtrader-frontend

2. Install the dependencies:
   npm install

3. Start the Vue.js development server:
   npm run serve

4. Access the application in your browser at `http://localhost:8080`.

## Usage

1. Register a new user account through the registration page.
2. Log in using your credentials.
3. Explore the watchlist to search and add stocks to track.
4. Perform simulated stock trading by buying and selling stocks.
5. Monitor your portfolio performance and account balance on the dashboard.

## License

Feel free to modify the contents of the README.md file to fit your specific application and add any additional sections or information as needed.
