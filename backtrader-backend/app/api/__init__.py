from flask import Flask
from flask_cors import CORS
from .authentication import authentication_bp
from .dashboard import dashboard_bp
from .watchlist import watchlist_bp
from .orders import orders_bp

app = Flask(__name__)
CORS(app)

app.register_blueprint(authentication_bp, url_prefix='/api/auth')
app.register_blueprint(dashboard_bp, url_prefix='/api/dashboard')
app.register_blueprint(watchlist_bp, url_prefix='/api/watchlist')
app.register_blueprint(orders_bp, url_prefix='/api/orders')

if __name__ == "__main__":
    app.run()
