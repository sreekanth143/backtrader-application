from flask import Blueprint, request, jsonify

from app.models.stock import Stock
from app.jwt_token import token_required

stocks_bp = Blueprint('stocks', __name__)


@stocks_bp.route('/stocks', methods=['GET'])
def get_stocks():
    stocks = Stock.get_all_stocks()
    return {'stocks': [Stock.serialize(stock) for stock in stocks]}


# Create
@stocks_bp.route('/create', methods=['POST'])
@token_required
def create_stock():
    data = request.get_json()
    new_stock = Stock.create_stock(**data)
    return jsonify(new_stock.serialize()), 201


@stocks_bp.route('/<string:stock_id>', methods=['GET'])
def get_stock(stock_id):
    stock = Stock.get_stock(stock_id)
    return jsonify(stock.serialize()) if stock else ('', 404)


@stocks_bp.route('/<string:stock_id>', methods=['PUT'])
@token_required
def update_stock(stock_id):
    data = request.get_json()
    stock = Stock.update_stock(stock_id, **data)
    return jsonify(stock.serialize()) if stock else ('', 404)


@stocks_bp.route('/<string:stock_id>', methods=['DELETE'])
@token_required
def delete_stock(stock_id):
    result = Stock.delete_stock(stock_id)
    return ('', 204) if result else ('', 404)
