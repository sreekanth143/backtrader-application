from flask import Flask
from flask_pymongo import PyMongo
from app.mailers.mailer import mail
from flask_cors import CORS

from config import DevelopmentConfig

mongo = PyMongo()


def create_app():
    app = Flask(__name__)
    app.config.from_object(DevelopmentConfig)

    mongo.init_app(app)
    mail.init_app(app)

    CORS(app)

    # Import and register blueprints
    from app.api.authentication import auth_bp
    from app.api.dashboard import dashboard_bp
    from app.api.orders import order_bp
    from app.api.stocks import stocks_bp
    from app.api.user import user_bp

    app.register_blueprint(auth_bp)
    app.register_blueprint(dashboard_bp)
    app.register_blueprint(order_bp)
    app.register_blueprint(stocks_bp)
    app.register_blueprint(user_bp)

    return app
