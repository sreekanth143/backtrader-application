import re
import os
import dotenv

dotenv.load_dotenv()

uri = "mongodb+srv://sreekanthvue:ZnM2ukopGtbj0o9B@cluster0.cun2h1u.mongodb.net/"
match = re.search(r'@([^:]+)', uri)
if match:
    hostname = match.group(1)
    print(hostname)


class Config:
    SECRET_KEY = os.environ.get('SECRET_KEY', 'my_precious_secret_key')
    MONGO_URI = os.environ.get(
        'MONGO_URI', 'mongodb://localhost:27017/your-database-name')

    # Email configuration
    MAIL_SERVER = os.environ.get('MAIL_SERVER', 'smtp.googlemail.com')
    MAIL_PORT = int(os.environ.get('MAIL_PORT', '587'))
    MAIL_USE_TLS = os.environ.get('MAIL_USE_TLS', 'True') == 'True'
    MAIL_USERNAME = os.environ.get('MAIL_USERNAME')
    MAIL_PASSWORD = os.environ.get('MAIL_PASSWORD')
    MAIL_USE_SSL = False
    MAIL_DEFAULT_SENDER = 'declare.game@gmail.com'


class DevelopmentConfig(Config):
    DEBUG = True


class ProductionConfig(Config):
    DEBUG = False


config_by_name = dict(
    dev=DevelopmentConfig,
    prod=ProductionConfig
)
